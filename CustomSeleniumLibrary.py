# CustomSeleniumLibrary.py
from Selenium2Library import Selenium2Library

class CustomSeleniumLibrary(Selenium2Library):
    def insert_into_prompt(self, text):
        alert = None
        try:
            alert = self._current_browser().switch_to_alert()
            alert.send_keys(text)

        except WebDriverException:
            raise RuntimeError('There were no alerts')